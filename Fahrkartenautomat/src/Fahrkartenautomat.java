﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       
      
       double zuZahlen, rueckgabewert;

       do
       {
	       zuZahlen = fahrkahrtenbestellungErfassen();
	       rueckgabewert = fahrkartenBezahlen(zuZahlen);
	       fahrkahrtenAusgeben();
	       rueckgeldAusgeben(rueckgabewert); 
	
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
       }while(zuZahlen != 0);
    }
    // Fahrkartenbestellung erfassen
    public static double fahrkahrtenbestellungErfassen()
    {
    	String[] ticketbezeichnung = new String[10];
    	double[] ticketpreise = new double[10];
    	double gesamtsumme = 0 , zwischenergebnis = 0;
    	int ticketauswahl;
    	int ticketanzahl = 0;
    	Scanner eingabe = new Scanner(System.in);
    	ticketbezeichnung[0] = "Einzelfahrschein Berlin AB";
    	ticketbezeichnung[1] = "Einzelfahrschein Berlin BC";
    	ticketbezeichnung[2] = "Einzelfahrschein Berlin ABC";
    	ticketbezeichnung[3] = "Kurzstrecke";
    	ticketbezeichnung[4] = "Tageskarte Berlin AB";
    	ticketbezeichnung[5] = "Tageskarte Berlin BC";
    	ticketbezeichnung[6] = "Tageskarte Berlin ABC";
    	ticketbezeichnung[7] = "Kleingruppen-Tageskarte Berlin AB";
    	ticketbezeichnung[8] = "Kleingruppen-Tageskarte Berlin BC";
    	ticketbezeichnung[9] = "Kleingruppen-Tageskarte Berlin ABC";
    	
    	ticketpreise[0] = 2.90;
    	ticketpreise[1] = 3.30;
    	ticketpreise[2] = 3.60;
    	ticketpreise[3] = 1.90;
    	ticketpreise[4] = 8.60;
    	ticketpreise[5] = 9.00;
    	ticketpreise[6] = 9.60;
    	ticketpreise[7] = 23.50;
    	ticketpreise[8] = 24.30;
    	ticketpreise[9] = 24.90;
    	
    	do
    	{
	
		    	System.out.print("Wählen Sie: \n");
		    	System.out.printf("(1) %1s %15.2f €\n",ticketbezeichnung[0], ticketpreise[0]);
		    	System.out.printf("(2) %s %15.2f €\n",ticketbezeichnung[1], ticketpreise[1]);
		    	System.out.printf("(3) %s %14.2f €\n",ticketbezeichnung[2], ticketpreise[2]);
		    	System.out.printf("(4) %s %30.2f €\n",ticketbezeichnung[3], ticketpreise[3]);
		    	System.out.printf("(5) %s %21.2f €\n",ticketbezeichnung[4], ticketpreise[4]);
		    	System.out.printf("(6) %s %21.2f €\n",ticketbezeichnung[5], ticketpreise[5]);
		    	System.out.printf("(7) %s %20.2f €\n",ticketbezeichnung[6], ticketpreise[6]);
		    	System.out.printf("(8) %s %8.2f €\n",ticketbezeichnung[7], ticketpreise[7]);
		    	System.out.printf("(9) %s %8.2f €\n",ticketbezeichnung[8], ticketpreise[8]);
		    	System.out.printf("(10) %s %6.2f €\n",ticketbezeichnung[9], ticketpreise[9]);
		    	System.out.printf("(0) Bezahlen.");
		    	
		    	ticketauswahl = eingabe.nextInt();
		
		        do{
					    if(ticketauswahl != 0)
					    {
			        	System.out.print("Anzahl der Tickets: ");
					    ticketanzahl = eingabe.nextInt();
					    if(ticketanzahl < 1 || ticketanzahl > 10)
					    	System.out.println("Sie haben eine ungültige Auswahl getroffen. Bitte geben Sie erneut einen Wert ein.");
					    }
				   }while(ticketanzahl < 1 || ticketanzahl > 10);
		        
		        if(ticketauswahl != 0)
		        {
		        	zwischenergebnis = ticketanzahl * ticketpreise[ticketauswahl - 1];
		        	gesamtsumme += zwischenergebnis;
		        }
		        
		        
	        
    	}while(ticketauswahl != 0);

        
        
        return gesamtsumme;
    }
        
    //Fahrkartenbezahlung
    public static double fahrkartenBezahlen(double zuZahlen)
    {
    	double eingeworfeneMuenze, eingezahlterGesamtbetrag = 0, rueckgabebetrag = 0;
    	
    	Scanner eingabe = new Scanner(System.in);
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
        	
        	System.out.printf("Noch zu zahlen: %.2f Euro\n", zuZahlen - eingezahlterGesamtbetrag);
        	System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
        	eingeworfeneMuenze = eingabe.nextDouble();
        	eingezahlterGesamtbetrag += eingeworfeneMuenze;
        	rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
     	   
     	   
        }
    	return rueckgabebetrag;
    }
    //Fahrkartenausgabe
    public static void fahrkahrtenAusgeben()
    {
    	System.out.println("\nFahrschein wird ausgegeben");
    	for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
    	 System.out.println("\n\n");
    }
   
 // Rückgeldausgabe
    public static void rueckgeldAusgeben(double rueckgabe)

    {
    	double rueckgabebetrag = rueckgabe;
    	
    	if(rueckgabebetrag >= 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO ", rueckgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rueckgabebetrag -= 1.0;
            }
            while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rueckgabebetrag -= 0.2;
            }
            while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rueckgabebetrag -= 0.1;
            }
            while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rueckgabebetrag -= 0.05;
            }
        } 
    }
    
    public static int warte(int millisekunde)
    {
    	try {
 			Thread.sleep(millisekunde);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
    	return 0;
    }
}