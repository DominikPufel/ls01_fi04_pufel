import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte fuer x und y festlegen:
      // ===========================
      double x, y;
      double m;
      
      
      x = einlesen("Bitte geben Sie den ersten Wert ein: ");
      y = einlesen("Bitte geben Sie den zweiten Wert ein: ");
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = mittelwert(x, y);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
   
   public static double einlesen(String frage)
   {
	   double zahl;
	   Scanner eingabe = new Scanner(System.in);
	   System.out.println(frage);
	   zahl = eingabe.nextDouble();
	  
	   return zahl;
	   
   }

private static double mittelwert(double x, double y) {
	double m;
	m = (x + y) / 2.0;
	return m;
}
}
